import { Component, Inject, Renderer2 } from '@angular/core';
import { Http } from '@angular/http';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private _httpService: Http, private _renderer2: Renderer2, @Inject(DOCUMENT) private _document) { }
    apiValues: Int32Array[] = [];
    drawingShape: string;
    chosenShape: string;

    public drawShape(chosenShape: string) {
        this._httpService.get('api/message/' + chosenShape).subscribe(values => {
            this.apiValues = values.json() as Int32Array[];
            //script
            let shapeId = Number(this.apiValues[0]);
            let x = Number(this.apiValues[1]);
            let y = Number(this.apiValues[2]);
            let width = Number(this.apiValues[3]);
            let height = Number(this.apiValues[4]);
            let dpi = window.devicePixelRatio;
            var c = <HTMLCanvasElement>document.getElementById("myCanvas");
            var ctx = c.getContext('2d');
            ctx.clearRect(0, 0, c.width, c.height);
            ctx.beginPath();
            var canvasHeight = 500;

            //Isosceles triangle
            if (shapeId == 0) {
                this.drawingShape = "Isosceles Traiangle";
                //to extend the height of canvas to fit shape
                c.height = height + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 2, y);
                ctx.closePath();
            }

            //square
            else if (shapeId == 1) {
                this.drawingShape = "Square";
                c.height = width + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.rect(x, y, width, width);
            }

            //scalene triangle
            else if (shapeId == 2)
            {
                this.drawingShape = "Scalene Traiangle";
                c.height = height + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 3, y);
                ctx.closePath();
            }

            //parallelogram
            else if (shapeId == 3) {
                this.drawingShape = "Parallelogram";
                c.height = width + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.moveTo(x, y);
                ctx.lineTo(x + width, y);
                ctx.lineTo(x - 20 + width, y + width);
                ctx.lineTo(x - 20, y + width);
                ctx.closePath();
            }

            //equilateral traingle
            else if (shapeId == 4) {
                this.drawingShape = "Equilateral Traiangle";
                c.height = width + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                var triangleHeight = width * (Math.sqrt(3) / 2);
                ctx.translate(c.width / 2, c.height/2);
                ctx.moveTo(0, -triangleHeight / 2);
                ctx.lineTo(-width / 2, triangleHeight / 2);
                ctx.lineTo(width / 2, triangleHeight / 2);               
                ctx.closePath();
            }

            //pentagon
            else if (shapeId == 5) {
                this.drawingShape = "Pentagon";
                c.height = width * 3;
                //function to clear blurriness
                canvasHeightandWidth();
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));

                for (side = 0; side < 6; side++) {
                    console.log(x  + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 5), y + width * Math.sin(side * 2 * Math.PI / 5));
                }
            }

            //rectangle
            else if (shapeId == 6) {
                this.drawingShape = "Rectangle";
                c.height = height + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.rect(x, y, width, height);
                ctx.rect(x + width / 2, y + height / 2, width, height);
                ctx.moveTo(x, y);
                ctx.lineTo(x + width / 2, y + height / 2);

                ctx.moveTo(x + width, y);
                ctx.lineTo(x + width + width / 2, y + height / 2);

                ctx.moveTo(x + width, y + height);
                ctx.lineTo(x + width + width / 2, y + height + height / 2);

                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width / 2, y + height + height / 2);                
            }

            //hexagon
            else if (shapeId == 7) {
                this.drawingShape = "Hexagon";
                c.height = width * 3;
                //function to clear blurriness
                canvasHeightandWidth();
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));

                for (side = 0; side < 7; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 6), y + width * Math.sin(side * 2 * Math.PI / 6));
                }
            }

            //heptagon
            else if (shapeId == 8) {
                this.drawingShape = "Heptagon";
                c.height = width * 4;
                //function to clear blurriness
                canvasHeightandWidth();
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));

                for (side = 0; side < 8; side++) {
                    console.log(x  + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 7), y + width * Math.sin(side * 2 * Math.PI / 7));
                }
            }

            //octagon
            else if (shapeId == 9) {
                this.drawingShape = "Octagon";
                var side = 0;
                c.height = width * 4;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));

                for (side = 0; side < 9; side++) {
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 8), y + width * Math.sin(side * 2 * Math.PI / 8));
                }
            }

            //circle
            else if (shapeId == 10) {
                this.drawingShape = "Circle";
                c.height = width * 2 + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();                     
                ctx.arc(x + width, y + width, width, 0, 2 * Math.PI);
            }

            //oval
            else if (shapeId == 11)
            {
                this.drawingShape = "Oval";
                c.height = width + y * 2;
                //function to clear blurriness
                canvasHeightandWidth();
                ctx.moveTo(x + width, y); // A1
                ctx.bezierCurveTo(
                    x + width + width / 2, y, // C1
                    x + width + width / 2, y + height, // C2
                    x + width, y + height); // A2

                ctx.bezierCurveTo(
                x + width - width / 2, y + height, // C3
                x + width - width / 2, y, // C4
                x + width, y); // A1
                ctx.closePath();
            }
            ctx.lineWidth = 2;
            ctx.strokeStyle = "green";
            ctx.stroke();
            function canvasHeightandWidth() {
                let style_height = +getComputedStyle(c).getPropertyValue("height").slice(0, -2);
                //get CSS width
                let style_width = +getComputedStyle(c).getPropertyValue("width").slice(0, -2);
                //scale the canvas
                c.height = style_height * dpi;
                c.width = style_width * dpi;  
            }
        });
            
    }
}