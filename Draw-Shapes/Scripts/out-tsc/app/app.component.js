import * as tslib_1 from "tslib";
import { Component, Inject, Renderer2 } from '@angular/core';
import { Http } from '@angular/http';
import { DOCUMENT } from '@angular/platform-browser';
var AppComponent = /** @class */ (function () {
    function AppComponent(_httpService, _renderer2, _document) {
        this._httpService = _httpService;
        this._renderer2 = _renderer2;
        this._document = _document;
        this.apiValues = [];
    }
    AppComponent.prototype.drawShape = function (chosenShape) {
        var _this = this;
        this._httpService.get('api/message/' + chosenShape).subscribe(function (values) {
            _this.apiValues = values.json();
            //script
            var shapeId = Number(_this.apiValues[0]);
            var x = Number(_this.apiValues[1]);
            var y = Number(_this.apiValues[2]);
            var width = Number(_this.apiValues[3]);
            var height = Number(_this.apiValues[4]);
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext('2d');
            ctx.clearRect(0, 0, c.width, c.height);
            ctx.beginPath();
            var canvasHeight = 500;
            //Isosceles triangle
            if (shapeId == 0) {
                _this.drawingShape = "Isosceles Traiangle";
                c.height = height + y * 2;
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 2, y);
                ctx.closePath();
            }
            //square
            else if (shapeId == 1) {
                _this.drawingShape = "Square";
                c.height = width + y * 2;
                ctx.rect(x, y, width, width);
            }
            //scalene triangle
            else if (shapeId == 2) {
                _this.drawingShape = "Scalene Traiangle";
                c.height = height + y * 2;
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 3, y);
                ctx.closePath();
            }
            //parallelogram
            else if (shapeId == 3) {
                _this.drawingShape = "Parallelogram";
                c.height = width + y * 2;
                ctx.moveTo(x, y);
                ctx.lineTo(x + width, y);
                ctx.lineTo(x - 20 + width, y + width);
                ctx.lineTo(x - 20, y + width);
                ctx.closePath();
            }
            //equilateral traingle
            else if (shapeId == 4) {
                _this.drawingShape = "Equilateral Traiangle";
                c.height = width + y * 2;
                var triangleHeight = width * (Math.sqrt(3) / 2);
                ctx.translate(c.width / 2, c.height / 2);
                ctx.moveTo(0, -triangleHeight / 2);
                ctx.lineTo(-width / 2, triangleHeight / 2);
                ctx.lineTo(width / 2, triangleHeight / 2);
                ctx.closePath();
            }
            //pentagon
            else if (shapeId == 5) {
                _this.drawingShape = "Pentagon";
                c.height = width * 3;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 6; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 5), y + width * Math.sin(side * 2 * Math.PI / 5));
                }
            }
            //rectangle
            else if (shapeId == 6) {
                _this.drawingShape = "Rectangle";
                c.height = height + y * 2;
                ctx.rect(x, y, width, height);
                ctx.rect(x + width / 2, y + height / 2, width, height);
                ctx.moveTo(x, y);
                ctx.lineTo(x + width / 2, y + height / 2);
                ctx.moveTo(x + width, y);
                ctx.lineTo(x + width + width / 2, y + height / 2);
                ctx.moveTo(x + width, y + height);
                ctx.lineTo(x + width + width / 2, y + height + height / 2);
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width / 2, y + height + height / 2);
            }
            //hexagon
            else if (shapeId == 7) {
                _this.drawingShape = "Hexagon";
                c.height = width * 3;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 7; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 6), y + width * Math.sin(side * 2 * Math.PI / 6));
                }
            }
            //heptagon
            else if (shapeId == 8) {
                _this.drawingShape = "Heptagon";
                c.height = width * 4;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 8; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 7), y + width * Math.sin(side * 2 * Math.PI / 7));
                }
            }
            //octagon
            else if (shapeId == 9) {
                _this.drawingShape = "Octagon";
                var side = 0;
                c.height = width * 4;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 9; side++) {
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 8), y + width * Math.sin(side * 2 * Math.PI / 8));
                }
            }
            //circle
            else if (shapeId == 10) {
                _this.drawingShape = "Circle";
                c.height = width * 2 + y * 2;
                ctx.arc(x + width, y + width, width, 0, 2 * Math.PI);
            }
            //oval
            else if (shapeId == 11) {
                _this.drawingShape = "Oval";
                c.height = width + y * 2;
                ctx.moveTo(x + width, y); // A1
                ctx.bezierCurveTo(x + width + width / 2, y, // C1
                x + width + width / 2, y + height, // C2
                x + width, y + height); // A2
                ctx.bezierCurveTo(x + width - width / 2, y + height, // C3
                x + width - width / 2, y, // C4
                x + width, y); // A1
                ctx.closePath();
            }
            ctx.lineWidth = 2;
            ctx.strokeStyle = "green";
            ctx.stroke();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        tslib_1.__param(2, Inject(DOCUMENT)),
        tslib_1.__metadata("design:paramtypes", [Http, Renderer2, Object])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map