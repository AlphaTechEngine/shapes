(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Please enter the shape of your desire.</h1>\r\n<div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n        <textarea placeholder=\"e.g. Draw an Isosceles Triangle with a height of 100 and a width of 50\" rows=\"4\" col=\"50\" style=\"-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width: 100%;\" [(ngModel)]=\"chosenShape\"></textarea>\r\n        <br /><br />\r\n        <input type=\"button\" class=\"btn-primary\" value=\"Draw Shape\" (click)=\"drawShape(chosenShape)\" />\r\n    </div>\r\n    <div class=\"col-md-8\">\r\n        <h4>Supported shapes</h4>\r\n        <div class=\"col-md-4\">\r\n            <ul>\r\n                <li>Isosceles Triangle</li>\r\n                <li>Square</li>\r\n                <li>Scalene Triangle</li>\r\n                <li>Parallelogram</li>\r\n                <li>Equilateral Triangle</li>\r\n                <li>Pentagon</li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"col-md-8\">\r\n            <ul>\r\n                <li>Rectangle (<em>3D looking shape</em>)</li>\r\n                <li>Hexagon</li>\r\n                <li>Heptagon</li>\r\n                <li>Octagon</li>\r\n                <li>Circle</li>\r\n                <li>Oval</li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div *ngIf=\"apiValues\">\r\n    <h3>{{drawingShape}}</h3>\r\n    <canvas id=\"myCanvas\" style=\"border:1px solid #d3d3d3; width: 100%; min-height: 500px;\">\r\n        Your browser does not support the HTML5 canvas tag.\r\n    </canvas>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(_httpService, _renderer2, _document) {
        this._httpService = _httpService;
        this._renderer2 = _renderer2;
        this._document = _document;
        this.apiValues = [];
    }
    AppComponent.prototype.drawShape = function (chosenShape) {
        var _this = this;
        this._httpService.get('api/message/' + chosenShape).subscribe(function (values) {
            _this.apiValues = values.json();
            //script
            var shapeId = Number(_this.apiValues[0]);
            var x = Number(_this.apiValues[1]);
            var y = Number(_this.apiValues[2]);
            var width = Number(_this.apiValues[3]);
            var height = Number(_this.apiValues[4]);
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext('2d');
            ctx.clearRect(0, 0, c.width, c.height);
            ctx.beginPath();
            var canvasHeight = 500;
            //Isosceles triangle
            if (shapeId == 0) {
                _this.drawingShape = "Isosceles Traiangle";
                c.height = height + y * 2;
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 2, y);
                ctx.closePath();
            }
            //square
            else if (shapeId == 1) {
                _this.drawingShape = "Square";
                c.height = width + y * 2;
                ctx.rect(x, y, width, width);
            }
            //scalene triangle
            else if (shapeId == 2) {
                _this.drawingShape = "Scalene Traiangle";
                c.height = height + y * 2;
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width, y + height);
                ctx.lineTo(x + width / 3, y);
                ctx.closePath();
            }
            //parallelogram
            else if (shapeId == 3) {
                _this.drawingShape = "Parallelogram";
                c.height = width + y * 2;
                ctx.moveTo(x, y);
                ctx.lineTo(x + width, y);
                ctx.lineTo(x - 20 + width, y + width);
                ctx.lineTo(x - 20, y + width);
                ctx.closePath();
            }
            //equilateral traingle
            else if (shapeId == 4) {
                _this.drawingShape = "Equilateral Traiangle";
                c.height = width + y * 2;
                var triangleHeight = width * (Math.sqrt(3) / 2);
                ctx.translate(c.width / 2, c.height / 2);
                ctx.moveTo(0, -triangleHeight / 2);
                ctx.lineTo(-width / 2, triangleHeight / 2);
                ctx.lineTo(width / 2, triangleHeight / 2);
                ctx.closePath();
            }
            //pentagon
            else if (shapeId == 5) {
                _this.drawingShape = "Pentagon";
                c.height = width * 3;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 6; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 5), y + width * Math.sin(side * 2 * Math.PI / 5));
                }
            }
            //rectangle
            else if (shapeId == 6) {
                _this.drawingShape = "Rectangle";
                c.height = height + y * 2;
                ctx.rect(x, y, width, height);
                ctx.rect(x + width / 2, y + height / 2, width, height);
                ctx.moveTo(x, y);
                ctx.lineTo(x + width / 2, y + height / 2);
                ctx.moveTo(x + width, y);
                ctx.lineTo(x + width + width / 2, y + height / 2);
                ctx.moveTo(x + width, y + height);
                ctx.lineTo(x + width + width / 2, y + height + height / 2);
                ctx.moveTo(x, y + height);
                ctx.lineTo(x + width / 2, y + height + height / 2);
            }
            //hexagon
            else if (shapeId == 7) {
                _this.drawingShape = "Hexagon";
                c.height = width * 3;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 7; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 6), y + width * Math.sin(side * 2 * Math.PI / 6));
                }
            }
            //heptagon
            else if (shapeId == 8) {
                _this.drawingShape = "Heptagon";
                c.height = width * 4;
                var side = 0;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 8; side++) {
                    console.log(x + width);
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 7), y + width * Math.sin(side * 2 * Math.PI / 7));
                }
            }
            //octagon
            else if (shapeId == 9) {
                _this.drawingShape = "Octagon";
                var side = 0;
                c.height = width * 4;
                ctx.moveTo(x + width * Math.cos(0), y + width * Math.sin(0));
                for (side = 0; side < 9; side++) {
                    ctx.lineTo(x + width * Math.cos(side * 2 * Math.PI / 8), y + width * Math.sin(side * 2 * Math.PI / 8));
                }
            }
            //circle
            else if (shapeId == 10) {
                _this.drawingShape = "Circle";
                c.height = width * 2 + y * 2;
                ctx.arc(x + width, y + width, width, 0, 2 * Math.PI);
            }
            //oval
            else if (shapeId == 11) {
                _this.drawingShape = "Oval";
                c.height = width + y * 2;
                ctx.moveTo(x + width, y); // A1
                ctx.bezierCurveTo(x + width + width / 2, y, // C1
                x + width + width / 2, y + height, // C2
                x + width, y + height); // A2
                ctx.bezierCurveTo(x + width - width / 2, y + height, // C3
                x + width - width / 2, y, // C4
                x + width, y); // A1
                ctx.closePath();
            }
            ctx.lineWidth = 2;
            ctx.strokeStyle = "green";
            ctx.stroke();
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"], Object])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_7__["HttpModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]
            ],
            providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_5__["APP_BASE_HREF"], useValue: '/' }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\shiva.magar\source\repos\Angular\Draw-Shapes\Draw-Shapes\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map