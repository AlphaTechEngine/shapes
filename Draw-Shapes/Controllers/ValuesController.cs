﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Draw_Shapes.Controllers
{
    public class ValuesController : ApiController
    {
        int width = 0;
        int height = 0;
        // GET api/<controller>
        [HttpGet]
        [Route("api/message/{chosenShape}")]
        public IEnumerable<int> Get(string chosenShape)
        {
            int shapeId = 0;
            int x = 50;
            int y = 50;

            if (chosenShape.ToLower().Contains("isosceles"))
            {
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("square"))
            {
                shapeId = 1;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("scalene"))
            {
                shapeId = 2;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("parallelogram"))
            {
                shapeId = 3;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("equilateral"))
            {
                shapeId = 4;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("pentagon"))
            {
                shapeId = 5;
                GetWidthandHeight(chosenShape, shapeId);
                x = x + width;
                y = x;
            }
            else if (chosenShape.ToLower().Contains("rectangle"))
            {
                shapeId = 6;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("hexagon"))
            {
                shapeId = 7;
                GetWidthandHeight(chosenShape, shapeId);
                x = x + width;
                y = x;
            }
            else if (chosenShape.ToLower().Contains("heptagon"))
            {
                shapeId = 8;
                GetWidthandHeight(chosenShape, shapeId);
                x = x + width;
                y = x;
            }
            else if (chosenShape.ToLower().Contains("octagon"))
            {
                shapeId = 9;
                GetWidthandHeight(chosenShape, shapeId);
                x = x + width;
                y = x;
            }
            else if (chosenShape.ToLower().Contains("circle"))
            {
                shapeId = 10;
                GetWidthandHeight(chosenShape, shapeId);
            }
            else if (chosenShape.ToLower().Contains("oval"))
            {
                shapeId = 11;
                GetWidthandHeight(chosenShape, shapeId);
            }
            return new int[] { shapeId, x, y, width, height };
        }

        public void GetWidthandHeight(string chosenShape, int shapeId)
        {
            Regex onlyNumber = new Regex(@"\d+");
            Regex regexWidth = new Regex(@"width of (\d+)");
            Regex regexHeight = new Regex(@"height of (\d+)");
            //array which may will contain both width and height
            int[] number = { 0, 2, 4, 6, 11 };
            string tempValue = "";
            if (number.Contains(shapeId))
            {
                //width value 
                string tempWidth = regexWidth.Match(chosenShape).Value;
                if (!String.IsNullOrEmpty(tempWidth))
                {
                    tempValue = onlyNumber.Match(tempWidth).Value;
                    if (!String.IsNullOrEmpty(tempValue))
                        width = Convert.ToInt32(tempValue);
                }
                //Height value
                string tempHeight = regexHeight.Match(chosenShape).Value;
                if (!String.IsNullOrEmpty(tempHeight))
                {
                    tempValue = onlyNumber.Match(tempHeight).Value;
                    if (!String.IsNullOrEmpty(tempValue))
                        height = Convert.ToInt32(tempValue);
                }
            }
            else
            {
                tempValue = onlyNumber.Match(chosenShape).Value;
                if (!String.IsNullOrEmpty(tempValue))
                    width = Convert.ToInt32(tempValue);
            }
        }
    }
}